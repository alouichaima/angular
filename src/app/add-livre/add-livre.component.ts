import { Component, OnInit } from '@angular/core';
import {Livre} from '../model/livre';
import {Router} from '@angular/router';


@Component({
  selector: 'app-add-livre',
  templateUrl: './add-livre.component.html',
  styleUrls: ['./add-livre.component.scss']
})
export class AddLivreComponent implements OnInit {

    l: Livre = new Livre();
   constructor(private livreService: LivreService,
                private router: Router) {
    }
    ngOnInit(): void {

    }
    addlivre(): void
    {
      this.livreService.addLivre(this.l)
        .subscribe(data=>
        {
          console.log(data);
        }, error => console.log(error));
    }


    toLivre(): void
    {
        this.router.navigate(['/LivresList']);
    }



    onSubmit():void
{
this.livreService.addLivre(this.l).subscribe(
(data)=>console.log(data),

(error)=>console.log(error),
()=>this.toLivre()

);
console.log(this.l);

}

    retour(): void {
      this.router.navigate(['/livresList']);
    }


 }
